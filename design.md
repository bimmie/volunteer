# Software design


# Database



```
            +---------------+         +---------------+
            | Event         |         | Task          |
            |---------------|         |---------------|
            | - Title       |         | - Title       |
            | - Description |         | - Description |
            | - Location    |         | - Date        |
            |               |         | - Start time  |
            |               |         | - End time    |
            |               |         | - Volunteer   |
            |               |         |               |
            |               |         |               |
            |               |         |               |
            |               |         |               |
            +---------------+         +---------------+
```

- GET /events
- GET /event/<eid:int>
- POST /event
- POST /event/<eid:int>/task
- POST /task/<tid:int>/volunteer
- DELETE /event/<eid:int>
- DELETE /event/<eid:int>/task/<tid:int>
- DELETE /event/<eid:int>/volunteer/<tid:int>
