module Main (main) where

import Prelude hiding ((/))

import Control.Monad.Trans.Class (lift)
import Crud as Crud
import Data.Either (Either(..))
import Data.Generic.Rep (class Generic)
import Effect (Effect)
import Effect.Class.Console (log)
import HTTPurple ((/))
import HTTPurple as HP
import HTTPurple.Headers as HPH
import HTTPurple.Json.Argonaut (jsonDecoder, jsonEncoder)
import Model (Event, Task, Volunteer, EventCreate)
import Record (merge)

type EventId = String
type TaskId = String
type VolunteerId = String

data Route
  = AllEvents
  | CreateEvent
  | GetEventDetail EventId
  | CreateTask EventId
  | AddVolunteer EventId TaskId
  | DeleteVolunteer VolunteerId

derive instance Generic Route _

route :: HP.RouteDuplex' Route
route = HP.mkRoute
  { "AllEvents": "events" / HP.noArgs
  , "GetEventDetail": "event" / HP.string HP.segment
  , "CreateEvent": "event" / HP.noArgs
  , "CreateTask": "event" / HP.string HP.segment / "task"
  , "AddVolunteer": "event" / HP.string HP.segment / "task" / HP.string HP.segment / "volunteer"
  , "DeleteVolunteer": "volunteer" / HP.string HP.segment
  }

headers :: HPH.ResponseHeaders
headers =
  (HPH.header "Access-Control-Allow-Origin" "*")
    <> (HPH.header "Access-Control-Allow-Headers" "*")
    <> (HPH.header "Access-Control-Allow-Methods" "*")
    <> HP.jsonHeaders

main :: Effect Unit
main = do
  database <- Crud.connectDB
  _ <- HP.serve { port: 8080 } { route, router: router database }
  pure unit

  where
  router db { method: HP.Get, route: AllEvents } = HP.usingCont do
    events <- lift $ Crud.allEvents db
    case events of
      Left error ->
        HP.badRequest' headers $ HP.toJson jsonEncoder { error }
      Right allEvents ->
        HP.ok' headers $ HP.toJson jsonEncoder allEvents

  router db { method: HP.Get, route: GetEventDetail id } = HP.usingCont do
    event <- lift $ Crud.eventDetail db id
    case event of
      Left error ->
        HP.badRequest' headers $ HP.toJson jsonEncoder { error }
      Right theEvent ->
        HP.ok' headers $ HP.toJson jsonEncoder theEvent

  router db { method: HP.Post, route: CreateEvent, body } = HP.usingCont do
    event :: EventCreate <- HP.fromJson jsonDecoder body
    id <- lift $ Crud.createEvent db event
    HP.ok' headers $ HP.toJson jsonEncoder $ merge { id } event

  router db { method: HP.Post, route: CreateTask eid, body } = HP.usingCont do
    task :: Task () <- HP.fromJson jsonDecoder body
    id <- lift $ Crud.createTask db eid task
    HP.ok' headers $ HP.toJson jsonEncoder $ merge { id } task

  router db { method: HP.Post, route: AddVolunteer eid tid, body } = HP.usingCont do
    volunteer :: Volunteer () <- HP.fromJson jsonDecoder body
    id <- lift $ Crud.addVolunteer db eid tid volunteer
    case id of
      Left error -> do
        log $ show error
        HP.badRequest' headers $ HP.toJson jsonEncoder { error }
      Right vid -> HP.ok' headers $ HP.toJson jsonEncoder $
        merge { id: vid, task: tid, event: eid } volunteer

  router db { method: HP.Delete, route: DeleteVolunteer volunteer } = HP.usingCont do
    result <- lift $ Crud.deleteVolunteer db volunteer
    case result of
      Left error -> do
        log $ show error
        HP.badRequest' headers $ HP.toJson jsonEncoder { error }
      Right _ -> HP.ok' headers $ HP.toJson jsonEncoder {}

  router _ { method: HP.Options } = HP.ok' headers "OK"

  router _ _ = HP.notFound
