module Crud
  ( DBConnection
  , allEvents
  , connectDB
  , createEvent
  , createTask
  , eventDetail
  , addVolunteer
  , deleteVolunteer
  ) where

import Prelude

import Control.Monad.Except (Except, runExcept)
import Control.Promise (Promise, toAffE)
import Data.Bifunctor (lmap)
import Data.Either (Either)
import Data.Foldable (intercalate)
import Data.Traversable (traverse)
import Effect (Effect)
import Effect.Aff (Aff)
import Foreign (Foreign, MultipleErrors, readArray, readInt, readNull, readString, renderForeignError)
import Foreign.Index ((!))
import Model
  ( Event
  , EventCreate
  , EventDetails
  , EventWithId
  , Task
  , TaskWithId
  , Volunteer
  , VolunteerWithDetails
  )

foreign import data DBConnection :: Type
foreign import connectDB :: Effect DBConnection
foreign import _createEvent :: DBConnection -> String -> String -> String -> Effect (Promise String)
foreign import _allEvents :: DBConnection -> Effect (Promise Foreign)
foreign import _getEvent :: DBConnection -> String -> Effect (Promise Foreign)
foreign import _createTask :: DBConnection -> String -> String -> String -> String -> String -> String -> Int -> Effect (Promise String)
foreign import _addVolunteer :: DBConnection -> String -> String -> String -> Effect (Promise Foreign)
foreign import _deleteVolunteer :: DBConnection -> String -> Effect (Promise Foreign)

-- toEventWithId :: Foreign -> Either String EventWithId
-- toEventWithId value = lmap ((intercalate "\n") <<< map renderForeignError) $ runExcept $ toEventWithId' value

toEventWithId' :: Foreign -> Except MultipleErrors EventWithId
toEventWithId' value = do
  id <- value ! "key" >>= readString
  name <- value ! "name" >>= readString
  description <- value ! "description" >>= readString
  location <- value ! "location" >>= readString
  pure { id, name, description, location }

toTaskWithId' :: Foreign -> Except MultipleErrors TaskWithId
toTaskWithId' value = do
  id <- value ! "key" >>= readString
  name <- value ! "name" >>= readString
  description <- value ! "description" >>= readString
  date <- value ! "date" >>= readString
  start_time <- value ! "start_time" >>= readString
  end_time <- value ! "end_time" >>= readString
  num_volunteers <- value ! "num_volunteers" >>= readInt
  pure { id, name, description, date, start_time, end_time, num_volunteers }

errorAsString :: forall a. Either MultipleErrors a -> Either String a
errorAsString = lmap ((intercalate "\n") <<< map renderForeignError)

toEventDetails :: Foreign -> Either String EventDetails
toEventDetails =
  errorAsString <<< runExcept <<< eventDetails
  where
  eventDetails :: Foreign -> Except MultipleErrors EventDetails
  eventDetails value = do
    tasks <- value ! "tasks" >>= readArray >>= traverse toTaskWithId'
    volunteers <- value ! "volunteers" >>= readArray >>= traverse volunteer
    { id, name, description, location } <- toEventWithId' value
    pure { event: id, name, description, location, tasks, volunteers }

  volunteer :: Foreign -> Except MultipleErrors VolunteerWithDetails
  volunteer value = do
    id <- value ! "key" >>= readString
    task <- value ! "task" >>= readString
    name <- value ! "name" >>= readString
    event <- value ! "event" >>= readString
    pure { id, name, task, event }

arrayOfEvents :: Foreign -> Either String (Array EventWithId)
arrayOfEvents value = errorAsString $ runExcept (readArray value >>= traverse toEventWithId')

createEvent :: DBConnection -> EventCreate -> Aff String
createEvent db event = do
  id <- toAffE $ _createEvent db event.name event.description event.location
  _ <- traverse (createTask db id) event.tasks
  pure id

allEvents :: DBConnection -> Aff (Either String (Array EventWithId))
allEvents = map arrayOfEvents <<< toAffE <<< _allEvents

eventDetail :: DBConnection -> String -> Aff (Either String EventDetails)
eventDetail db id =
  map toEventDetails (toAffE $ _getEvent db id)

createTask :: DBConnection -> String -> Task () -> Aff String
createTask db event task =
  toAffE $ _createTask db event task.name task.description task.date task.start_time task.end_time task.num_volunteers

addVolunteer :: DBConnection -> String -> String -> Volunteer () -> Aff (Either String String)
addVolunteer db event task volunteer =
  map (errorAsString <<< runExcept <<< readString) $ toAffE $ _addVolunteer db event task volunteer.name

deleteVolunteer :: DBConnection -> String -> Aff (Either String Unit)
deleteVolunteer db volunteer =
  map (map (const unit) <<< errorAsString <<< runExcept <<< readNull)
    $ toAffE
    $ _deleteVolunteer db volunteer
