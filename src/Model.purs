module Model where

type Event k =
  { name :: String
  , description :: String
  , location :: String
  | k
  }

type EventWithId = Event (id :: String)

type EventCreate = Event (tasks :: Array (Task ()))

type EventDetails =
  { event :: String
  , name :: String
  , description :: String
  , location :: String
  , tasks :: Array TaskWithId
  , volunteers :: Array VolunteerWithDetails
  }

type Task a =
  { name :: String
  , description :: String
  , date :: String
  , start_time :: String
  , end_time :: String
  , num_volunteers :: Int
  | a
  }

type TaskWithId = Task (id :: String)

type Volunteer a =
  { name :: String
  | a
  }

type VolunteerWithId = Volunteer (id :: String)

type VolunteerWithDetails = Volunteer (id :: String, task :: String, event :: String)
