import { Deta } from "deta";

export const connectDB = () => {
  const project_key = process.env.DETA_PROJECT_KEY;
  const deta = project_key ? Deta(project_key) : Deta();
  return {
    events: deta.Base("events"),
    tasks: deta.Base("tasks"),
    volunteers: deta.Base("volunteers"),
  };
};

export const _createEvent =
  (database) => (name) => (description) => (location) => {
    return () => {
      return database.events
        .put({ name, description, location })
        .then((result) => result.key);
    };
  };

export const _allEvents = (database) => {
  return () => {
    return database.events.fetch().then(({ items }) => items);
  };
};

export const _getEvent = (database) => (id) => {
  return () => {
    return database.events.get(id).then((event) => {
      return database.tasks.fetch({ event: id }).then((tasks) => {
        return database.volunteers.fetch({ event: id }).then((volunteers) => ({
          ...event,
          tasks: tasks.items,
          volunteers: volunteers.items,
        }));
      });
    });
  };
};

export const _createTask =
  (database) =>
  (event) =>
  (name) =>
  (description) =>
  (date) =>
  (start_time) =>
  (end_time) =>
  (num_volunteers) => {
    return () => {
      return database.tasks
        .put({
          event,
          name,
          description,
          date,
          start_time,
          end_time,
          num_volunteers,
        })
        .then(({ key }) => key);
    };
  };

export const _addVolunteer = (database) => (event) => (task) => (name) => {
  return () => {
    return database.volunteers
      .put({ event, task, name })
      .then(({ key }) => key);
  };
};

export const _deleteVolunteer = (database) => (volunteer) => {
  return () => {
    return database.volunteers.delete(volunteer);
  };
};
