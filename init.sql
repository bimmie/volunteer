CREATE TABLE IF NOT EXISTS events (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       name TEXT NOT NULL,
       description TEXT,
       location TEXT
);

CREATE TABLE IF NOT EXISTS tasks (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       event INTEGER NOT NULL,
       name TEXT NOT NULL,
       description TEXT,
       date TEXT NOT NULL,
       start_time TEXT NOT NULL,
       end_time TEXT NOT NULL,
       num_volunteers INTEGER NOT NULL,
       FOREIGN KEY(event) REFERENCES events(id)
);

CREATE TABLE IF NOT EXISTS volunteers (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       task INTEGER NOT NULL,
       name TEXT NOT NULL,
       FOREIGN KEY(task) REFERENCES tasks(id)
);